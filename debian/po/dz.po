# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of dz.po to Dzongkha
# Translation of debian-installer level 1 Dzongkha
# Debian Installer master translation file template
# Copyright @ 2006 Free Software Foundation, Inc.
# Sonam Rinchen <somchen@druknet.bt>, 2006.
#
#
# Translations from iso-codes:
#   Free Software Foundation, Inc., 2006
#   Kinley Tshering <gaseokuenden2k3@hotmail.com>, 2006
#
msgid ""
msgstr ""
"Project-Id-Version: dDz.po\n"
"Report-Msgid-Bugs-To: partman-hfs@packages.debian.org\n"
"POT-Creation-Date: 2022-03-23 14:39+0100\n"
"PO-Revision-Date: 2021-05-21 05:32+0000\n"
"Last-Translator: Jacque Fresco <aidter@use.startmail.com>\n"
"Language-Team: Dzongkha <pgeyleg@dit.gov.bt>\n"
"Language: dz\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-hfs.templates:1001 ../partman-hfs.templates:3001
#, fuzzy
msgid "HFS"
msgstr "ཛེཌི་ཨེཕ་ཨེསི།"

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-hfs.templates:2001
msgid "Hierarchical File System"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-hfs.templates:4001 ../partman-hfs.templates:6001
msgid "HFS+"
msgstr ""

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-hfs.templates:5001
msgid "Hierarchical File System Plus"
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:7001
msgid "Go back to the menu and correct this problem?"
msgstr "དཀར་ཆག་ལུ་ལོག་འགྱོ་འདི་དཀའ་ངལ་འདི་ནོར་བཅོས་འབད?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:7001
#, fuzzy
msgid ""
"You have not configured an HFS partition that is mounted to /boot/grub. This "
"is needed by your machine in order to be able to boot. Please go back and "
"create an HFS partition that is mounted to /boot/grub."
msgstr ""
"ཁྱོད་ཀྱི་སྲ་ཆས་ཌིཀསི་ཀྱི་བར་བཅད་གཞི་རིམ་དང་པ་ནང་ལུ་ཁྱོད་ཀྱི་བུཊི་བར་བཅད་འདི་མ་གནས་པས་ འ་ནི་འདི་བུཊི་"
"གོ་རིམ་འབད་ནི་ལུ་ཁྱོད་ཀྱི་གློག་འཕྲུལ་གྱིས་འགོཔ་ཨིན་ ལོག་འགྱོ་འདི་བུཊི་བར་བཅད་བཟུམ་སྦེ་ཁྱོད་ཀྱི་བར་བཅད་གཞི་"
"རིམ་དང་པ་འདི་ལག་ལེན་འཐབ་གནང་།"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:7001
msgid ""
"If you do not go back to the partitioning menu and correct this error, the "
"partition will be used as is. This means that you may not be able to boot "
"from your hard disk."
msgstr ""
"ཁྱོད་ཀྱིས་བར་བཅད་འབད་མིའི་དཀར་ཆག་ནང་ལུ་ལོག་མ་འགྱོཝ་དང་འཛོལ་བ་འདི་ནོར་བཅོས་མ་འབད་བ་ཅིན་ བར་"
"བཅད་འདི་ག་ཨིནམ་སྦེ་ལག་ལེན་འཐབ་ནི་ འ་ནི་འདི་ཁྱོད་ཀྱི་སྲ་ཆས་ཌིཀསི་ནང་ལས་ཁྱོད་ཀྱིས་བུཊི་འབད་མི་ཚུགསཔ་"
"འོང་ཟེར་བའི་དོན་ཨིན།"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:8001
msgid "Do you want to return to the partitioning menu?"
msgstr "ཁྱོད་ བར་བཅད་འབད་མིའི་དཀར་ཆག་ནང་ལུ་སླར་ལོག་འབད་ནི་ཨིན་ན?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:8001
msgid ""
"The partition ${PARTITION} assigned to ${MOUNTPOINT} starts at an offset of "
"${OFFSET} bytes from the minimum alignment for this disk, which may lead to "
"very poor performance."
msgstr ""
"${MOUNTPOINT} ལུ་འགན་སྤྲོད་འབད་ཡོད་པའི་ བར་བཅད་ ${PARTITION} འདི་ ${OFFSET} བཱའིཊི་ གི་"
"པར་ལེན་ལས་ ཌིཀསི་འདི་གི་ཕྲང་སྒྲིག་ཉུང་མཐའ་ལས་འགོ་བཙུགས་ཨིན་ཏེ་ ལཱ་ཤུགས་མེད་པའི་རྐྱེན་འབྱུང་སྲིད།"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:8001
msgid ""
"Since you are formatting this partition, you should correct this problem now "
"by realigning the partition, as it will be difficult to change later. To do "
"this, go back to the main partitioning menu, delete the partition, and "
"recreate it in the same position with the same settings. This will cause the "
"partition to start at a point best suited for this disk."
msgstr ""
"ཁྱོད་ཀྱིས་ བར་བཅད་འདི་རྩ་སྒྲིག་འབད་ནི་ཨིནམ་ལས་ ལོག་ཤུལ་ལས་སྔོན་སྒྲིག་སྦེ་སོར་བཏུབ་ལས་ སྟབས་མ་བདེཝ་འདི་"
"ད་ལྟོ་ལས་ བར་བཅད་ལོག་ཕྲང་སྒྲིག་འབད་དེ་ ནོར་བཅོས་འབད། འདི་འབད་ནིའི་དོན་ལུ་ བར་བཅད་གཙོ་བོ་དཀར་"
"ཆག་ལུ་འགྱོ་ བར་བཅད་བཏོན་གཏང་ དེ་ལས་ སྒྲིག་སྟངས་གཅིག་པའི་ཐོག་ གནས་ཁོངས་འདི་ན་རང་ ལོག་གསར་བསྐྲུན་"
"འབད། འདི་གིས་ ཌིཀསི་འདི་ལུ་ལྡན་པའི་ས་ཚིག་དྲག་ཤོས་ནང་ལུ་ བར་བཅད་འགོ་བཙུགས་འོང་།"
