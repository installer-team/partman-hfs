# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_mk.po to Macedonian
# translation of mk.po to
# Macedonian strings from the debian-installer.
#
# Georgi Stanojevski, <glisha@gmail.com>, 2004, 2005, 2006.
# Georgi Stanojevski <georgis@unet.com.mk>, 2005, 2006.
# Kristijan Fremen Velkovski <me@krisfremen.com>, 2023.
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@computer.org>, 2002
#   Arangel Angov <ufo@linux.net.mk>, 2008.
#   Free Software Foundation, Inc., 2002,2004
#   Georgi Stanojevski <glisha@gmail.com>, 2004, 2006.
# Translations from KDE:
#   Danko Ilik <danko@mindless.com>
# Arangel Angov <arangel@linux.net.mk>, 2008, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_mk\n"
"Report-Msgid-Bugs-To: partman-hfs@packages.debian.org\n"
"POT-Creation-Date: 2022-03-23 14:39+0100\n"
"PO-Revision-Date: 2024-06-04 07:09+0000\n"
"Last-Translator: Kristijan Fremen Velkovski <me@krisfremen.com>\n"
"Language-Team: Macedonian <>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n!=1);\n"

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-hfs.templates:1001 ../partman-hfs.templates:3001
msgid "HFS"
msgstr "HFS"

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-hfs.templates:2001
msgid "Hierarchical File System"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-hfs.templates:4001 ../partman-hfs.templates:6001
msgid "HFS+"
msgstr "HFS+"

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-hfs.templates:5001
msgid "Hierarchical File System Plus"
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:7001
msgid "Go back to the menu and correct this problem?"
msgstr "Врати се во менито и поправи го проблемот?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:7001
#, fuzzy
msgid ""
"You have not configured an HFS partition that is mounted to /boot/grub. This "
"is needed by your machine in order to be able to boot. Please go back and "
"create an HFS partition that is mounted to /boot/grub."
msgstr ""
"Твојата бут партиција не е лоцирана на првата примарна партиција од тврдиот "
"диск. Ова е потребно за да може да се бутира твојата машина. Те молам врати "
"се и искористи ја твојата прва примарна партиција како бут партиција."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:7001
msgid ""
"If you do not go back to the partitioning menu and correct this error, the "
"partition will be used as is. This means that you may not be able to boot "
"from your hard disk."
msgstr ""
"Ако не се вратиш во мениот за партиционирање и ја поправиш оваа грешка, "
"партицијата ќе се користи како што е. Ова значи дека можеби нема да можеш да "
"подигнеш од твојот тврд диск."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:8001
msgid "Do you want to return to the partitioning menu?"
msgstr "Дали сакаш да се вратиш во менито за партиционирање?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:8001
msgid ""
"The partition ${PARTITION} assigned to ${MOUNTPOINT} starts at an offset of "
"${OFFSET} bytes from the minimum alignment for this disk, which may lead to "
"very poor performance."
msgstr ""
"Партицијата ${PARTITION} доделена на ${MOUNTPOINT} започнува со офсет од "
"${OFFSET} бајти од минималното порамнување за овој диск. Ова може да "
"придонесе за спори перформанси."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:8001
msgid ""
"Since you are formatting this partition, you should correct this problem now "
"by realigning the partition, as it will be difficult to change later. To do "
"this, go back to the main partitioning menu, delete the partition, and "
"recreate it in the same position with the same settings. This will cause the "
"partition to start at a point best suited for this disk."
msgstr ""
"Бидејќи ја форматираш оваа партиција, треба да го поправиш овој проблем сега "
"преку усогласување на партицијата, бидејќи ќе биде многу потешко да го "
"направиш тоа подоцна. Врати се назад во главното мени за партиционирање, "
"избриши ја партицијата и рекреирај ја на истата позиција со истите "
"поставувања. Ова ќе резултира со поставување на партицијата на место каде "
"што најдобро одговара."
